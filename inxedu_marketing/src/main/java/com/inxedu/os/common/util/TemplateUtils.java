package com.inxedu.os.common.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inxedu.os.edu.entity.Template.TemplateFile;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Administrator 文件管理操作实用类
 * @author www.inxedu.com
 */
public class TemplateUtils {
	/**
	 * 某路径下的文件集合
	 */
	public static List<Map<String,Object>> 	queryTemplateFile(String filepath){
		List<Map<String,Object>> folderNameList = new ArrayList<Map<String,Object>>();;
		//模板路径
		String templets = filepath;
		File file = new File(templets);
		String[] str = file.list();
		if (!StringUtils.isBlank(str)) {
			for (int i = 0; i < str.length; i++) {
				File readfile = new File(filepath + "/" + str[i]);
				Map map = new HashMap();
				//判断文件是文件夹还是文件
				if (!readfile.isDirectory()) {
					map.put("type","file");
					//截取后缀
					if(StringUtils.isNotEmpty(readfile.getName())){
						String suffixName = readfile.getName().trim().toLowerCase();
						if (suffixName.length()!=0){
							suffixName = suffixName.substring(suffixName.lastIndexOf("."));
							map.put("suffixName",suffixName);
						}
					}
				} else if (readfile.isDirectory()) {
					map.put("type","Folder");
				}
				map.put("path",readfile.getPath());
				map.put("absolutepath",readfile.getAbsolutePath());
				map.put("name",readfile.getName());
				folderNameList.add(map);

			}
		}
		return folderNameList;
	}

	/**
	 * 读取某个文件夹下的所有文件
	 */
	public static String readfile(String filepath) {

		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
		List<TemplateFile> list = new ArrayList<TemplateFile>();
		addFileList(list,filepath);
		return gson.toJson(list);
	}

	//递归查询某文件夹目录下全部的文件
	public static void addFileList(List<TemplateFile> list, String filepath){
		File file = new File(filepath);
		if (!file.isDirectory()) {
			System.out.println("文件");
			System.out.println("path=" + file.getPath());
			System.out.println("absolutepath=" + file.getAbsolutePath());
			System.out.println("name=" + file.getName());

		} else if (file.isDirectory()) {
			TemplateFile templateFile = new TemplateFile();
			System.out.println("文件夹");
			templateFile.setName(file.getName());
			templateFile.setFilePath(file.getPath());
			templateFile.setFileType("file");
			list.add(templateFile);
			String[] filelist = file.list();
			for (int i = 0; i < filelist.length; i++) {
				File readfile = new File(filepath + "/" + filelist[i]);
				if (!readfile.isDirectory()) {
					System.out.println("path=" + readfile.getPath());
					System.out.println("absolutepath="
							+ readfile.getAbsolutePath());
					System.out.println("name=" + readfile.getName());
					TemplateFile templateFileson = new TemplateFile();
					templateFileson.setName(readfile.getName());
					templateFileson.setFilePath(readfile.getPath());
					templateFileson.setFileType("file");
					templateFileson.setParentFileName(file.getName());
					list.add(templateFile);
				} else if (readfile.isDirectory()) {
					addFileList(list,filepath + "/" + filelist[i]);
				}
			}

		}
	}
}
