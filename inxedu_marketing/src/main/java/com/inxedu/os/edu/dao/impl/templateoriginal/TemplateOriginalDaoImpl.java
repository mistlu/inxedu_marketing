package com.inxedu.os.edu.dao.impl.templateoriginal;

import java.util.List;
import com.inxedu.os.common.entity.PageEntity;
import com.inxedu.os.edu.entity.templateoriginal.TemplateOriginal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.inxedu.os.common.dao.GenericDaoImpl;
import com.inxedu.os.edu.dao.templateoriginal.TemplateOriginalDao;

/**
 * @author www.inxedu.com
 * @description 模板原始文件 TemplateOriginalDao接口实现
 */
@Repository("templateOriginalDao")
public class TemplateOriginalDaoImpl extends GenericDaoImpl implements TemplateOriginalDao{
	/**
     * 添加模板原始文件
     */
    public Long addTemplateOriginal(TemplateOriginal templateOriginal){
    	this.insert("TemplateOriginalMapper.addTemplateOriginal", templateOriginal);
		return templateOriginal.getId();
    }
    
    /**
     * 删除模板原始文件
     * @param id
     */
    public void delTemplateOriginalById(Long id){
    	this.update("TemplateOriginalMapper.delTemplateOriginalById", id);
    }
    
    /**
     * 修改模板原始文件
     * @param templateOriginal
     */
    public void updateTemplateOriginal(TemplateOriginal templateOriginal){
    	this.update("TemplateOriginalMapper.updateTemplateOriginal", templateOriginal);
    }
    
    /**
     * 通过id，查询模板原始文件
     * @param id
     * @return
     */
    public TemplateOriginal getTemplateOriginalById(Long id){
    	return this.selectOne("TemplateOriginalMapper.getTemplateOriginalById", id);
    }
    
    /**
     * 分页查询模板原始文件列表
     * @param templateOriginal 查询条件
     * @param page 分页条件
     * @return List<TemplateOriginal>
     */
    public List<TemplateOriginal> queryTemplateOriginalListPage(TemplateOriginal templateOriginal,PageEntity page){
    	return this.queryForListPage("TemplateOriginalMapper.queryTemplateOriginalListPage", templateOriginal, page);
    }
    
    /**
     * 条件查询模板原始文件列表
     * @param templateOriginal 查询条件
     * @return List<TemplateOriginal>
     */
    public List<TemplateOriginal> queryTemplateOriginalList(TemplateOriginal templateOriginal){
    	return this.selectList("TemplateOriginalMapper.queryTemplateOriginalList", templateOriginal);
    }
}



