package com.inxedu.os.edu.entity.websiteimagesdefault;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
/**
 * @author www.inxedu.com
 * @description 模板默认数据（还原用，后期根据方案优化）
 */
@Data
public class WebsiteImagesDefault implements Serializable {
    private static final long serialVersionUID = 3148176768559230877L;

	/** imageId */
	private Long imageId;
	/** 图片地址 */
	private String imageUrl;
	/** 图链接地址 */
	private String linkAddress;
	/** 图标题 */
	private String title;
	/** 图片类型 */
	private int typeId;
	/** 序列号 */
	private int seriesNumber;
	/** 略缩图片地址 */
	private String previewUrl;
	/** 背景色 */
	private Date publishTime;
	/** 图片描述 */
	private String describe;
	/** 页面模板路径（还原用，根据路径加载数据，重新添加记录） */
	private String webpageTemplateUrl;

}

