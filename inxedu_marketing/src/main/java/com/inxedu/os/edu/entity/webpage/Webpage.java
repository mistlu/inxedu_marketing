package com.inxedu.os.edu.entity.webpage;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author www.inxedu.com
 * @description 页面
 */
@Data
public class Webpage implements Serializable {
    private static final long serialVersionUID = 3148176768559230877L;

	/** id */
	private Long id;
	/** 页面名称 */
	private String pageName;
	/** 标题 */
	private String title;
	/** 作者 */
	private String author;
	/** 关键词 */
	private String keywords;
	/** 描述 */
	private String description;
	/** 发布地址 */
	private String publishUrl;
	/** 创建时间 */
	private Date createTime;
	/** 是否不发布首页 */
	private boolean notPublishIndex=false;

}

