package com.inxedu.os.edu.controller.main;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.inxedu.os.common.cache.CacheUtil;
import com.inxedu.os.common.constants.CacheConstans;
import com.inxedu.os.common.constants.CommonConstants;
import com.inxedu.os.common.controller.BaseController;
import com.inxedu.os.common.util.MD5;
import com.inxedu.os.common.util.ObjectUtils;
import com.inxedu.os.common.util.SingletonLoginUtils;
import com.inxedu.os.common.util.WebUtils;
import com.inxedu.os.edu.constants.enums.WebSiteProfileType;
import com.inxedu.os.edu.entity.system.SysUser;
import com.inxedu.os.edu.entity.system.SysUserLoginLog;
import com.inxedu.os.edu.entity.webpage.Webpage;
import com.inxedu.os.edu.entity.website.WebsiteProfile;
import com.inxedu.os.edu.service.system.SysUserLoginLogService;
import com.inxedu.os.edu.service.system.SysUserService;
import com.inxedu.os.edu.service.webpage.WebpageService;
import com.inxedu.os.edu.service.website.WebsiteProfileService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author www.inxedu.com
 *
 */
@Controller
@RequestMapping("/admin")
public class LoginController extends BaseController{
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	private static String loginPage = getViewPath("/admin/main/login");
	private static String loginSuccess="redirect:/admin/main";
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysUserLoginLogService sysUserLoginLogService;
	@Autowired
	private WebsiteProfileService websiteProfileService;
	@Autowired
	private WebpageService webpageService;
	@InitBinder({"sysUser"})
	public void initBinderSysUser(WebDataBinder binder) {
		binder.setFieldDefaultPrefix("sysUser.");
	}
	
	/**
	 * 后台用户退出登录
	 * @param request
	 * @param response
	 */
	@RequestMapping("/outlogin")
	public String outLogin(HttpServletRequest request,HttpServletResponse response){
		try{
			request.getSession().invalidate();
			int userId = SingletonLoginUtils.getLoginSysUserId(request);
			//删除所有的权限缓存
			CacheUtil.remove(CacheConstans.SYS_ALL_USER_FUNCTION_PREFIX+userId);
			//删除登录用户的缓存信息
			CacheUtil.remove(CacheConstans.LOGIN_MEMCACHE_PREFIX+userId);
			//删除登录用户权限缓存
			CacheUtil.remove(CacheConstans.USER_FUNCTION_PREFIX+userId);
			//删除页面用户Cookie
			WebUtils.deleteCookie(request, response, CacheConstans.LOGIN_MEMCACHE_PREFIX);
			//清空所有的Session
			request.getSession().invalidate();


			//退出时获取 最后一次选择的首页主题 模板 存储到数据库
			Webpage webpage=new Webpage();
			webpage.setPublishUrl("/index.html");
			List<Webpage> webpageList=webpageService.queryWebpageList(webpage);
			if (ObjectUtils.isNotNull(webpageList)) {
				webpage = webpageList.get(0);
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("defaultTemplateId", webpage.getId()+"");// 默认页面模板id
				// 将map转化json串
				JsonParser jsonParser = new JsonParser();
				JsonObject jsonObject = jsonParser.parse(gson.toJson(map)).getAsJsonObject();
				if (ObjectUtils.isNotNull(jsonObject) && com.inxedu.os.common.util.StringUtils.isNotEmpty(jsonObject.toString())) {// 如果不为空进行更新
					WebsiteProfile websiteProfile = new WebsiteProfile();// 创建websiteProfile
					websiteProfile.setType(WebSiteProfileType.defaultTemplateId.toString());
					websiteProfile.setDesciption(jsonObject.toString());
					websiteProfileService.updateWebsiteProfile(websiteProfile);
				}
			}
		}catch (Exception e) {
			logger.error("outLogin()---error",e);
			return this.setExceptionRequest(request, e);
		}
		return "redirect:/admin";
	}
	/**
	 * 进入登录页面
	 */
	@RequestMapping
	public ModelAndView loginPage(HttpServletRequest request){
		ModelAndView model = new ModelAndView();
		try{
			model.setViewName(loginPage);
		}catch (Exception e) {
			model.setViewName(this.setExceptionRequest(request, e));
			logger.error("loginPage()--error",e);
		}
		return model;
	}
	/**
	 * 进入教师登录页面
	 */
	@RequestMapping("/tlogin")
	public String teacherLogin(HttpServletRequest request){

		return getViewPath("/admin/main/teacher_login");
	}

	/**
	 * 执行登录
	 * @param request
	 * @return ModelAndView
	 */
	@RequestMapping("/main/login")
	public ModelAndView login(HttpServletRequest request,HttpServletResponse response,@ModelAttribute("sysUser") SysUser sysUser){
		ModelAndView model = new ModelAndView();
		try{
			model.setViewName(loginPage);
			model.addObject("sysUser", sysUser);
			if(sysUser.getLoginName()==null || sysUser.getLoginName().trim().equals("")){
				model.addObject("message", "请输入用户名!");
				return model;
			}
			if(sysUser.getLoginPwd()==null || sysUser.getLoginPwd().trim().equals("")){
				model.addObject("message", "请输入密码!");
				return model;
			}
			
			//获取Session中验证码
			String randCode = (String) request.getSession().getAttribute(CommonConstants.RAND_CODE);
			//用户输入的验证码
			String randomCode = request.getParameter("randomCode");
			if(randomCode==null || !randomCode.equals(randCode)){
				model.addObject("message", "验证码不正确！");
				return model;
			}
			request.getSession().removeAttribute(CommonConstants.RAND_CODE);
			sysUser.setLoginPwd(MD5.getMD5(sysUser.getLoginPwd()));
			SysUser su = sysUserService.queryLoginUser(sysUser);
			if(su==null){
				model.addObject("message", "用户名或密码错误！");
				return model;
			}
			//判断用户是否是可用状态
			if(su.getStatus()!=0){
				model.addObject("message", "该用户已经冻结！");
				return model;
			}
			//缓存用登录信息
			CacheUtil.set(CacheConstans.LOGIN_MEMCACHE_PREFIX+su.getUserId(), su);
			//request.getSession().setAttribute(CacheConstans.LOGIN_MEMCACHE_PREFIX+su.getUserId(),su );
			WebUtils.setCookie(response, CacheConstans.LOGIN_MEMCACHE_PREFIX, CacheConstans.LOGIN_MEMCACHE_PREFIX+su.getUserId(), 1);
			
			//修改用户登录记录
			sysUserService.updateUserLoginLog(su.getUserId(), new Date(), WebUtils.getIpAddr(request));
			
			//添加登录记录
			SysUserLoginLog loginLog = new SysUserLoginLog();
			loginLog.setUserId(su.getUserId());//用户ID
			loginLog.setLoginTime(new Date());//
			loginLog.setIp(WebUtils.getIpAddr(request));//登录IP
			String userAgent = WebUtils.getUserAgent(request);
			if(StringUtils.isNotEmpty(userAgent)){
				loginLog.setUserAgent(userAgent.split(";")[0]);//浏览器
				loginLog.setOsName(userAgent.split(";")[1]);//操作系统
			}
			//保存登录日志
			sysUserLoginLogService.createLoginLog(loginLog);
			model.setViewName(loginSuccess);
		}catch (Exception e) {
			model.addObject("message", "系统繁忙，请稍后再操作！");
			logger.error("login()--error",e);
		}
		return model;
	}
	/**
	 * 跳转找回密码页面
	 *//*
	@RequestMapping("/passwordRecovery")
	public String passWordRecovery(){
		return getViewPath("/admin/main/password-recovery");
	}
	*//**
	 * 发送找回密码验证码
	 *//*
	@RequestMapping("/sendCode")
	@ResponseBody
	public Map<String,Object> sendCode(HttpServletRequest request){
		Map<String, Object> json = new HashMap<String, Object>(4);
		try {
			*//*根据登陆名查找用户信息*//*
			String loginName = request.getParameter("userName");
			SysUser sysUser = new SysUser();
			sysUser.setLoginName(loginName);
			SysUser revoceryPwdUser = sysUserService.queryUserByLoginName(sysUser);
			*//*如果查到用户为null用户不存在，程序返回*//*
			if (ObjectUtils.isNull(revoceryPwdUser)){
				json = this.setJson(false,"该用户不存在!",null);
				return json;
			}
			String code = WebUtils.getRandomNum(4);
			CacheUtil.set("emailCodeNum",code,300);
			emailService.sendMail("密码找回","您的验证码是:"+code+",有效时间为5分钟。",revoceryPwdUser.getEmail());
			json = this.setJson(true,"验证码以邮件形式发送至您的邮箱，请注意查收",null);
		}catch (Exception e) {
			logger.error("LoginController.sendCode()---error", e);
			json=this.setAjaxException(json);
		}
		return json;
	}
	*//**
	 * 跳转找回密码页面
	 *//*
	@RequestMapping("/recoverNewPwd")
	@ResponseBody
	public Map<String,Object> recoverNewPwd(HttpServletRequest request){
		Map<String,Object> json = new HashMap<String,Object>();
		try {
			String userLoginName = request.getParameter("userName");
			String code = request.getParameter("code");
			String newPwd = request.getParameter("newPwd");
			String confirmPwd = request.getParameter("confirmPwd");
			*//*根据登陆名查找用户信息*//*
			SysUser sysUser = new SysUser();
			sysUser.setLoginName(userLoginName);
			SysUser revoceryPwdUser = sysUserService.queryLoginUser(sysUser);
			*//*如果查到用户为null用户不存在，程序返回*//*
			if (ObjectUtils.isNull(revoceryPwdUser)){
				json = this.setJson(false,"该用户不存在!",null);
				return json;
			}
			if (!code.equals(CacheUtil.get("emailCodeNum").toString())){
				json = this.setJson(false,"请输入正确的验证码！",null);
			}
			//判断密码格式
			if(!WebUtils.isPasswordAvailable(newPwd)){
				json = this.setJson(false, "密码只能是数字字母组合且大于等于6位小于等于16位", null);
				return json;
			}
			if (!newPwd.equals(confirmPwd)){
				json = this.setJson(false,"新密码和确认密码不一致！",null);

			}
			sysUser.setLoginPwd(newPwd);
			sysUserService.updateSysUser(sysUser);
			json = this.setJson(true,"修改成功",null);
		}catch (Exception e) {
			logger.error("recoverNewPwd()--error",e);
		}
		return json;
	}*/
}
