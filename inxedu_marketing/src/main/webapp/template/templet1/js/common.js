//移动端导航显示与隐藏
var wmNavFun = function() {
    var wmBtn = $(".mw-nav-btn"),
        hmMask = $(".h-mobile-mask"),
        wH = $(window).height();
    $(".head-mobile").css("height", wH+"px");
    wmBtn.click(function() {
        if (!wmBtn.hasClass("mw-tap")) {
            $(this).addClass("mw-tap");
            $("html").addClass("active");
            hmMask.show().css("opacity","1");
        } else {
            $(this).removeClass("mw-tap");
            $("html").removeClass("active");
            hmMask.css("opacity","0").hide();
        }
    });
    hmMask.click(function() {
        if(!hmMask.is(":hidden")) {
            wmBtn.removeClass("mw-tap");
            $("html").removeClass("active");
            hmMask.css("opacity","0").hide();
        }
    });
};
//首页响应式幻灯片调取方法
function sSwiperFun() {
    var _sWrap = $('.banner-wrap .i-slide .swiper-container');
    var mySwiper = _sWrap.swiper({
        loop: true, //无缝连接滚动
        autoplay : 5000, //自动滚动
        autoplayDisableOnInteraction : false, //设置点击后是否继续滚动
        speed:300, //滚动速度
        pagination : '.banner-wrap .pagination', //设置分页
        paginationClickable :true //设置true分页点击执行swiper
    });
    $('.banner-wrap .arrow-left').on('click', function(e){
        e.preventDefault();
        mySwiper.swipePrev();
      });
    $('.banner-wrap .arrow-right').on('click', function(e){
        e.preventDefault();
        mySwiper.swipeNext();
    });

    if($(".imgload").length>0){
        $(".imgload").eq(0).get(0).onload=function(){
            $(".banner-wrap .i-slide").css("height",$(".imgload").eq(0).height());
        }
    }
    $(window).resize(function(){
        $(".banner-wrap .i-slide").css("height",$(".imgload").eq(0).height());
    });
}
//课程分类调取方法
function couSwiperFun() {
    var _sWrap = $('.cou-fl-slide .swiper-container');
    var mySwiper = _sWrap.swiper({
        loop: true, //无缝连接滚动
        autoplay : false, //自动滚动
        autoplayDisableOnInteraction : false, //设置点击后是否继续滚动
        speed:300, //滚动速度
        pagination : '.cou-fl-slide .pagination', //设置分页
        paginationClickable :true //设置true分页点击执行swiper
    });
    $('.cou-fl-slide .arrow-left').on('click', function(e){
        e.preventDefault();
        mySwiper.swipePrev();
      });
    $('.cou-fl-slide .arrow-right').on('click', function(e){
        e.preventDefault();
        mySwiper.swipeNext();
    });

     $(".cou-fl-slide").css("height",$(".cou-fl-slide .c-fl-pic").eq(0).height());
   
    $(window).resize(function(){
        $(".cou-fl-slide").css("height",$(".cou-fl-slide .c-fl-pic").eq(0).height());
    });
}
//课程分类调取方法
function d6SwiperFun() {
    var _sWrap = $('.d6-slider .swiper-container');
    var mySwiper = _sWrap.swiper({
        loop: true, //无缝连接滚动
        autoplay : false, //自动滚动
        autoplayDisableOnInteraction : false, //设置点击后是否继续滚动
        speed:300, //滚动速度
        pagination : false, //设置分页
        paginationClickable :true //设置true分页点击执行swiper
    });
    $('.d6-slider .arrow-left').on('click', function(e){
        e.preventDefault();
        mySwiper.swipePrev();
      });
    $('.d6-slider .arrow-right').on('click', function(e){
        e.preventDefault();
        mySwiper.swipeNext();
    });
 
    $(".d6-slider").css("height",$(".demo-list4>ul").height()); 
    $(window).resize(function(){
        $(".d6-slider").css("height",$(".demo-list4>ul").eq(0).height());
    });
}
// scrollLoad 滚动响应加载调用图片方法
var scrollLoad = (function (options) {
    var defaults = (arguments.length == 0) ? { src: 'xSrc', time: 500} : { src: options.src || 'xSrc', time: options.time ||500};
    var camelize = function (s) {
        return s.replace(/-(\w)/g, function (strMatch, p1) {
            return p1.toUpperCase();
        });
    };
    this.getStyle = function (element, property) {
        if (arguments.length != 2) return false;
        var value = element.style[camelize(property)];
        if (!value) {
            if (document.defaultView && document.defaultView.getComputedStyle) {
                var css = document.defaultView.getComputedStyle(element, null);
                value = css ? css.getPropertyValue(property) : null;
            } else if (element.currentStyle) {
                value = element.currentStyle[camelize(property)];
            }
        }
        return value == 'auto' ? '' : value;
    };
    var _init = function () {
        var offsetPage = window.pageYOffset ? window.pageYOffset : window.document.documentElement.scrollTop,   //滚动条滚动高度
            offsetWindow = offsetPage + Number(window.innerHeight ? window.innerHeight : document.documentElement.clientHeight),
            docImg = document.getElementById("aCoursesList").getElementsByTagName("img"),           //通过ID查找获取图片节点
            _len = docImg.length;
        if (!_len) return false;
        for (var i = 0; i < _len; i++) {
            var attrSrc = docImg[i].getAttribute(defaults.src),
                o = docImg[i], tag = o.nodeName.toLowerCase();
            if (o) {
                postPage = o.getBoundingClientRect().top + window.document.documentElement.scrollTop + window.document.body.scrollTop;
                postWindow = postPage + Number(this.getStyle(o, 'height').replace('px', ''));   
                if ((postPage > offsetPage && postPage < offsetWindow) || (postWindow > offsetPage && postWindow < offsetWindow)) { //判断元素始终在可见区域内
                    if (tag === "img" && attrSrc !== null) {
                        o.setAttribute("src", attrSrc);
                    }
                    o = null;
                }
            }
        }
        window.onscroll = function () {
            setTimeout(function () {
                _init();
            }, defaults.time);
        }
    };
    return _init();
});
//切换文字
function cardChange(oTitle, oCont) {
    var oTitle = $(oTitle),
        oCont = $(oCont),
        _index;
    oTitle.hover(function() {
        _index = oTitle.index(this);
        $(this).addClass("current").siblings().removeClass("current");
        oCont.eq(_index).show().siblings().hide();
    })
}
// 公告滚动方法
function nSlideUp() {
    var _upWrap = $("ul.newsNotice"),
        _sTime = 5000,
        _moving;
    _upWrap.hover(function() {
        clearInterval(_moving);
    }, function() {
        _moving = setInterval(function() {
            var _mC = _upWrap.find("li:first");
            var _mH = _mC.height();
            _mC.animate({"margin-top" : -_mH + "px"}, 600, function() {
                _mC.css("margin-top", 0).appendTo(_upWrap);
            });
        }, _sTime);
    }).trigger("mouseleave");
}
//向上滚动方法
var upSlideFun = function(od) {
    var _upWrap = $(od),
          _sTime = 5000,
          _moving;
    _upWrap.hover(function() {
        clearInterval(_moving);
    }, function() {
        _moving = setInterval(function() {
            var _mC = _upWrap.find("li:first");
            var _mH = _mC.height();
            _mC.animate({"margin-top" : -_mH + "px"}, 600, function() {
                _mC.css("margin-top", 0).appendTo(_upWrap);
            });
        }, _sTime);
    }).trigger("mouseleave");
};
//demo6资讯滚动方法
 function cardChangeFun(oBox,oTitle, oCont, current, auto) {
          var oBox = $(oBox),
              oTitle = $(oTitle),
              oCont = $(oCont),
              _index;
             oTitle.hover(function() {
                _index = oTitle.index(this);
                $(this).addClass(current).siblings().removeClass(current);
                oCont.eq(_index).show().siblings().hide();
            }).eq(0).hover();
           // autoChange;
           var _i = 0;
           var _timer = null;
           var _oLi = oTitle.size();
           var changeFun = function(q) {
               _i = q;
               oTitle.eq(q).addClass(current).siblings().removeClass(current);
               oCont.eq(q).show().siblings().hide();
           };
           if (auto === true) {
                _timer = setInterval(function() {
                    _i++;
                    if (_i > _oLi) {
                        _i = 0;
                    }
                    changeFun(_i);
                }, 6000);
                
                oBox.hover(function() {
                    clearInterval(_timer);
                }, function() {
                    _timer = setInterval(function() {
                        _i++;
                        if (_i > _oLi) {
                            _i = 0;
                        }
                        changeFun(_i);
                    }, 6000);
                })
           }
        } 
/**
 * 滚动方法
 */
function scrollFunList(sfWrap) {
    var textDiv = document.getElementById(sfWrap);
    if(textDiv==undefined){
        return;
    }
    var textList = textDiv.getElementsByTagName("li");
    if(textList.length > 3){
        var textDat = textDiv.innerHTML;
        var br = textDat.toLowerCase().indexOf("<li",textDat.toLowerCase().indexOf("<li")+3);
        textDiv.innerHTML = textDat+textDat+textDat.substr(0,br);
        textDiv.style.cssText = "position:absolute; top:0";
        var textDatH = textDiv.offsetHeight;
        MaxRoll();
    }
    var minTime,maxTime,divTop,newTop=0;
    function MinRoll(){
        newTop++;
        if(newTop<=divTop+80){
        textDiv.style.top = "-" + newTop + "px";
        }else{
        clearInterval(minTime);
        maxTime = setTimeout(MaxRoll,100);
        }
    }
    function MaxRoll(){
        divTop = Math.abs(parseInt(textDiv.style.top));
        if(divTop>=0 && divTop<textDatH-300){
        minTime = setInterval(MinRoll,80);
        }else{
        textDiv.style.top = 0;divTop = 0;newTop=0;MaxRoll();
        }
    }
    textDiv.onmouseover = function() {
        clearInterval(minTime);
        clearTimeout(maxTime);
    };
    textDiv.onmouseout = function() {
        MinRoll();
        MaxRoll();
    }

}