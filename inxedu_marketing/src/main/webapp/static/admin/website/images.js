	$(function(){
		//初始化图片上传
		initSimpleImageUpload("imageFile","image",imgCallback);
		//初始化略缩图片上传
		initSimpleImageUpload("previewFile","image",previewCallback);
	});
	//图片上传回调
	function imgCallback(imgUrl){
		$("input[name='websiteImages.imagesUrl']").val(imgUrl);
		$("#imagesUrl").attr('src',imagesPath+imgUrl);
	}
	//略缩图片上传回调
	function previewCallback(imgUrl){
		$("input[name='websiteImages.previewUrl']").val(imgUrl);
		$("#previewUrl").attr('src',imagesPath+imgUrl);
	}
	
	/**
	 * 提交信息
	 */
	function saveImage(){
		var title = $("input[name='websiteImages.title']").val();
		if(title==null || $.trim(title)==''){
			msgshow("请填写图片标题",'false');
			return false;
		}
		var typeId = $("select[name='websiteImages.typeId']").val();
		if(typeId<=0){
			msgshow("请选择图片类型",'false');
			return false;
		}
		var sort = $("input[name='websiteImages.seriesNumber']").val();
		var reg=/^\d+$/;
		if(!reg.test(sort)){
			msgshow("排序只能填写正整数",'false');
			return false;
		}
		var imagesUrl =$("input[name='websiteImages.imagesUrl']").val();
		if(imagesUrl==null || $.trim(imagesUrl)==''){
			msgshow("请上传图片",'false');
			return false;
		}
		var mobile = $("#mobile").val();
		if (mobile!=""){
			mobile = "/"+mobile
		}
		var email = $("#email").val();
		if (email!=""){
			email = "/"+email
		}
		var location = $("#location").val();
		if (location!=""){
			location = "/"+location
		}
		var describe = $("#describe").val()+mobile+email+location;
		$("#describe").val(describe);
		$("#saveImagesForm").submit();
	}
