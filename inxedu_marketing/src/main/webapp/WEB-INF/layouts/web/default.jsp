<%@ page import="com.inxedu.os.common.constants.CacheConstans" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/base.jsp"%>
<!DOCTYPE HTML>
<!--[if lt IE 7]>      <html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><sitemesh:write property='title'/>-${websitemap.web.company}-${websitemap.web.title}</title>
<meta name="author" content="${websitemap.web.author}" />
<meta name="keywords" content="${websitemap.web.keywords}" />
<meta name="description" content="${websitemap.web.description}" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="${ctx}/static/mooc_web/css/reset.css">
<link rel="stylesheet" type="text/css" href="${ctx}/static/mooc_web/css/global.css">
<link rel="stylesheet" type="text/css" href="${ctx}/static/mooc_web/css/web.css">
<link href="${ctx}/static/mooc_web/css/mw_320_768.css" rel="stylesheet" type="text/css" media="screen and (min-width: 320px) and (max-width: 768px)">
<!--[if lt IE 9]><script src="${ctx}/static/common/html5.js"></script><![endif]-->
<script type="text/javascript" src="${ctx}/static/common/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${ctx}/static/common/webutils.js"></script>
<script type="text/javascript" src="${ctx }/static/mooc_web/js/common.js"></script>
<script type="text/javascript">
	var baselocation = "${ctx}";
	var imagesPath='<%=staticImage%>';
	var keuploadSimpleUrl='<%=keuploadSimpleUrl%>';
	var uploadServerUrl='${ctx}';
	var uploadSimpleUrl="<%=uploadSimpleUrl%>";
</script>
<sitemesh:write property='head'/>
</head>
<body class="W-body">
		<%--Cookie域--%>
	<input type="hidden" id="mydomain" value="<%=CommonConstants.MYDOMAIN%>"/>
	<%--变量--%>
	<c:if test="${WebSwitch.limitLogin=='ON'}"><%--限制登陆js--%>
		<script type="text/javascript" src="${ctx}/static/common/limit_login/limitLogin.js"></script>
	</c:if>


	<div class="in-wrap">
	<!-- 公共头引入 -->
	<jsp:include page="/WEB-INF/layouts/web/header.jsp" />
	<!-- 公共头引入 -->
	<sitemesh:write property='body'/>
	<!-- 公共底引入 -->
	<jsp:include page="/WEB-INF/layouts/web/footer.jsp" />
	<!-- 公共底引入 -->
	</div>
	<section style="color: #666;position:absolute;left:50%;bottom:20px;z-index:7;margin-left:-80px;"><span>Powered by</span><a style="margin-left: 2px;color: #666;" title="因酷教育软件" target="_blank" href="http://www.inxedu.com">因酷教育软件</a></section>
</body>
</html>
