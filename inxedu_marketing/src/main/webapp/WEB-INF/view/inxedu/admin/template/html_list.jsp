<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/8/30
  Time: 11:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/base.jsp"%>
<html>
<head>
    <title>模版列表</title>
    <style>
        img{
            width: 64px;
            height: 64px;
        }
        li{
            display: inline;
            margin-left: 50px;
        }
        ol{
            margin-top: 30px;
        }
    </style>
</head>
<body>
<table cellspacing="0" cellpadding="0" border="0" class="fullwidth">
    <thead>
    <tr>
        <td>文件名</td>
        <td>文件路径</td>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${htmlName}" var="htmlName" varStatus="varStatus">
        <tr <c:if test="${index.count%2==1 }">class="odd"</c:if>>
            <td><span id="file${varStatus.index}"class="fileName">${htmlName.key}</span></td>
            <td><a href="javascript: void(0)" onclick="getFileType('${htmlName.value}')">${fn:replace(htmlName.value,"\\" ,'\\')}</td>
        </tr>

    </c:forEach>
    </tbody>
</table>
    <script>
        function getFileType(name) {
            var fileType = name.substring(name.lastIndexOf(".")+1);
            if (fileType=="jsp" ||fileType=="html"|| fileType=="css"||fileType=="js"){
                window.location.href = baselocation+"/admin/template/toUpdateFile?path="+name;
            }
        }
    </script>
</body>
</html>
