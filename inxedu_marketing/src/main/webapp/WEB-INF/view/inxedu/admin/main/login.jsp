<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<!-- Meta -->
<meta charset="utf-8" http-equiv="Content-Type" />
<!-- End of Meta -->
<!-- Page title -->
<title>登录 -${websitemap.web.company}-${websitemap.web.title}</title>
<!-- End of Page title -->
<meta name="author" content="${websitemap.web.author}" />
<meta name="keywords" content="${websitemap.web.keywords}" />
<meta name="description" content="${websitemap.web.description}" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<script type="text/javascript" src="${ctx}/static/common/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="${ctx}/static/common/webutils.js"></script>

<style>
	#message{width: 374px;height: 65px;position: absolute;left: 50%;}
	.msg-nr{border-radius: 6px;overflow: hidden;}
	.msg-nr p{font-size: 20px;line-height: 63px;text-align: center;margin: 0;}
	.msg-nr-zq{background: url('/static/admin/assets/green-bg.png') repeat top left;border: 1px solid #8fab83;}
	.msg-nr-zq p{color: #468747;}
	.msg-nr-cw{background: url('/static/admin/assets/red-bg.png') repeat top left;border: 1px solid #ff5423;}
	.msg-nr-cw p{color: #ff5423;}
</style>
	<!-- Libraries -->
<link type="text/css" href="${ctx}/static/admin/css/login.css" rel="stylesheet" />
<script type="text/javascript">
			function enterSubmit(event) {
				var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
				if (keyCode == 13) {
					$("#loginForm").submit();
					return false;
				}
			}
            /*按键盘登陆后台验证码错误*/
			function submitLogin() {
				$("#loginForm").submit();
			}
			function recoveryPwd() {
				$("#loginForm").hide()
				$("#recoveryPwd").show()
			}
			/*找回密码提交*/
			function submitform(){
				if ($("#userName")==""){
					alert(00);
					msgshow("请输入用户名！",false)
					return
				}
				if ($("#code")==""){
					msgshow("请输入验证码！",false)
					return
				}
				if ($("#newPwd")==""){
					msgshow("请输入密码！",false)
					return
				}
				if ($("#confirmPwd")==""){
					msgshow("请输入确认密码！",false)
					return
				}
				$.ajax({
					url:'/admin/recoveryPwd/recoverNewPwd',
					type:'post',
					data:{"userName":$("#userName").val(),"code":$("#code").val(),"newPwd":$("#newPwd").val(),"confirmPwd":$("#confirmPwd").val()},
					async:false,
					dataType:'json',
					success:function(result){
						if(!result.success){
							msgshow(result.message,"false")
						}else {
							if (confirm(result.message,"true")){
								window.location.href = "/admin"
							}
						}
					}
				});
			}
			/*发送验证码*/
			function sendCode() {
				var userName=$("#userName").val()
				if (userName==""){
					msgshow("请输入用户名！",false)
					return
				}
				$.ajax({
					url:'/admin/recoveryPwd/sendCode',
					type:'post',
					data:{"userName":$("#userName").val()},
					async:false,
					dataType:'json',
					success:function(result){
						if(!result.success){
							msgshow(result.message,"false")
						}else {
							msgshow(result.message,"true");
						}
					}
				});
			}
		</script>
</head>
<body>
	<div id="container">
		<div class="logo">
			<a href="http://demo1.inxedu.com/" target="_blank" title="因酷在线教育软件 - 在线教育整体解决方案提供商">
				<img src="${ctx}/static/admin/assets/logo.png" width="200" alt="因酷在线教育软件 - 在线教育整体解决方案提供商" />
			</a>
		</div>
	</div>

	<div class="b-box">
		<div id="container">
			<div id="box">
				<h2>网校后台管理系统</h2>
				<form action="${ctx}/admin/main/login" method="POST" id="loginForm">
					<p class="main">
						<label>用户名: </label>
						<input name="sysUser.loginName" onkeyup="enterSubmit(event)" value="${sysUser.loginName}" placeholder="输入用户名" />
						<label>密码: </label>
						<input type="password" onkeyup="enterSubmit(event)" name="sysUser.loginPwd" value="${sysUser.loginPwd}" placeholder="输入密码">
					</p>
					<p class="main">
						<label>验证码: </label>
						<input name="randomCode" onkeyup="enterSubmit(event)" placeholder="验证码" style="width: 105px;"  maxlength="4"/>
						<span class="yzm-pic">
							<img src="${ctx}/ran/random" alt="验证码，点击图片更换" onclick="this.src='${ctx}/ran/random?random='+Math.random();" />
						</span>

					</p>
					<p class="space">
						<label class="wjpwd"><a onclick="recoveryPwd()" title="找回密码"><u>忘记密码？点击找回</u></a></label>
						<input type="button" onclick="submitLogin()" value="登录" class="login" />
						<span>${message}</span>
					</p>
				</form>
				<div id="recoveryPwd" style="display: none">
					<p class="zhpwd">
						<label for="sf"><font color="red"></font>&nbsp;用户名:</label>
						<input  name="userName" id="userName" class="{required:true} lf" data-rule="required;"  placeholder="输入用户名"/>
						<span class="field_desc"></span>
					</p>
					<p class="zhpwd">
						<label for="sf"><font color="red"></font>&nbsp;验证码:</label>
						<input style="width: 105px;"  name="code"id="code" class="{required:true} lf" data-rule="required;" placeholder="输入验证码"/>
						<span class="field_desc"></span>
						<button id="sendCode" class="sendCode" onclick="sendCode()" title="发送验证码">发送验证码</button>
					</p>
					<p class="zhpwd">
						<label for="sf"><font color="red"></font>&nbsp;设置密码：</label>
						<input type="password" name="newPwd" id="newPwd" class="{required:true,number:true,min:0,max:1000} lf" data-rule="required;" placeholder="输入密码"/>
						<span class="field_desc"></span>
					</p>
					<p class="zhpwd">
						<label for="sf"><font color="red"></font>&nbsp;确认密码：</label>
						<input type="password" name="confirmPwd" id="confirmPwd" class="{required:true} lf" data-rule="required;" placeholder="再次输入密码"/>
						<span class="field_desc"></span>
					</p>
					<p class="zhpwd btnbox">
						<input type="button" value="提 交" class="button btn-tj" onclick="submitform()" />
						<input type="button" value="返 回" class="button" onclick="javascript:history.go(-1);" />
					</p>

				</div>
			</div>
			<div class="login-foot">
				<span>
					Powered By <a target="_blank" href="http://www.inxedu.com/" style="color: #666;">${websitemap.web.company}</a>
				</span>
			</div>
		</div>
	</div>
</body>
</html>