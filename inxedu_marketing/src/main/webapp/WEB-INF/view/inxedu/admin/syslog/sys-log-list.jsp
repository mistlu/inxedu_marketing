<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/base.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>操作日志记录</title>
<script type="text/javascript" src="${ctx}/static/common/jquery-1.11.1.min.js"></script>
<script src="${ctx}/static/common/jquery-ui-1.10.4/js/jquery-ui-1.10.4.custom.js?v=${v}"></script>
<script src="${ctx}/static/common/jquery-ui-1.10.4/js/jquery.ui.datepicker-zh-CN.js?v=${v}"></script>
<script type="text/javascript" src="${ctx}/static/common/jquery-ui-1.10.4/js/jquery-ui-timepicker-addon.js?v=${v}"></script>
<script type="text/javascript" src="${ctx}/static/common/jquery-ui-1.10.4/js/jquery-ui-timepicker-zh-CN.js?v=${v}"></script>
<script>
	$(function(){
		$("#beginTime,#endTime").datetimepicker({
			regional:"zh-CN",
			changeMonth: true,
			dateFormat:"yy-mm-dd",
			timeFormat: "HH:mm:ss"
		});
	});
	function query(){
		$("#searchForm").submit();
	}
</script>
</head>
<body>
	<div class="">
		<form action="${ctx}/admin/syslog/page" method="post" id="searchForm">
		<caption>
			<div class="capHead">
				<div class="w50pre fl">
					<ul class="ddBar">
						<li>
							<span class="ddTitle"><font>操作人：</font></span>
							<input type="text"  id="loginName" name="sysLog.loginName" value="${sysLog.loginName}"/>
						</li>
						<li>
							<span class="ddTitle"><font>开始时间：</font></span>
							<input id="beginTime" type="text" readonly="readonly" name="sysLog.beginTime" value="<fmt:formatDate value="${sysLog.beginTime}" pattern="yyyy-MM-dd HH:mm:ss" />"  />
						</li>
					</ul>
				</div>
				<div class="w50pre fl">
					<ul class="ddBar">
						<li>
							<span class="ddTitle"><font>类型：</font></span>
							<select name="sysLog.type" id="type">
								<option value="">-- 全部 --</option>
								<option value="add">添加</option>
								<option value="update">更新</option>
								<option value="del">删除</option>
							</select>
							<script>
								$("#type").val("${sysLog.type}");
							</script>
						</li>
						<li>
							<span class="ddTitle"><font>结束时间：</font></span>

							<input id="endTime" type="text" name="sysLog.endTime" value="<fmt:formatDate value="${sysLog.endTime}" pattern="yyyy-MM-dd HH:mm:ss" />"/>
						</li>
					</ul>
				</div>
				<div class="w50pre fl" style="text-align: center;">
					<ul class="ddBar">
						<li>
							<input class="btn btn-danger ml10" type="button" onclick="query()" value="查询">
							<input class="btn btn-danger ml10" type="button" onclick="$('#searchForm input:text,#searchForm select').val('');" value="清空">
						</li>
					</ul>
				</div>
				<div class="clear"></div>
			</div>

		</caption>

			<input type="hidden" id="pageCurrentPage" name="page.currentPage" value="1" />
		</form>
		<table cellspacing="0" cellpadding="0" border="0" class="fullwidth">
			<thead>
				<tr>
					<td align="center">操作人</td>

					<td align="center">类型</td>
					<td align="center">操作描述</td>
					<td align="center">添加时间</td>
					<td align="center">操作</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${sysLogList}" var="syslog" varStatus="index">
					<tr <c:if test="${index.count%2==1 }">class="odd"</c:if>>

						<td align="center">${syslog.loginName}</td>

						<td align="center">
							<c:if test="${syslog.type=='add'}">添加</c:if>
							<c:if test="${syslog.type=='update'}">更新</c:if>
							<c:if test="${syslog.type=='del'}">删除</c:if>
						</td>
						<td align="center">${syslog.operation}</td>
						<td align="center">
							<fmt:formatDate value="${syslog.createTime}" pattern="yyyy-MM-dd HH:mm:ss" />
						</td>
						<td align="center">
							<button type="button" onclick="window.location.href='/admin/syslog/query?id=${syslog.id}'">查看</button>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<jsp:include page="/WEB-INF/view/common/admin_page.jsp" />
	</div>
</body>
</html>