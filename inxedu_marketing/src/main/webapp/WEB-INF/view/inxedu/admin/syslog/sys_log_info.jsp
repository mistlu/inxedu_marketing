<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ include file="/base.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>操作日志详情</title>
    <script type="text/javascript">
    </script>

</head>
<body>
    <input type="hidden" name="type" value="${type}" />
    <fieldset>
        <legend>
            <span>操作日志详情</span>
        </legend>
        <div class="mt20">
                <p>
                    <label ><font color="red">*</font>&nbsp;id</label>
                    ${sysLog.id}
                </p>
                <p>
                    <label ><font color="red">*</font>&nbsp;操作人账号</label>
                    ${sysLog.loginName}
                </p>
            <p>
                <label ><font color="red">*</font>&nbsp;操作人姓名</label>
                ${sysLog.userName}
            </p>
            <p>
                <label ><font color="red">*</font>&nbsp;类型</label>
                <c:if test="${sysLog.type=='add'}">添加</c:if>
                <c:if test="${sysLog.type=='update'}">更新</c:if>
                <c:if test="${sysLog.type=='del'}">删除</c:if>
            </p>
                <p>
                    <label ><font color="red">*</font>&nbsp;添加时间</label>
                    <fmt:formatDate value="${sysLog.createTime}" pattern="yyyy-MM-dd HH:mm:ss" />
                </p>
            <p>
                <label ><font color="red">*</font>&nbsp;操作描述</label>
                ${sysLog.operation}
            </p>
                <p>
                    <label ><font color="red">*</font>&nbsp;相关信息</label>
                    ${sysLog.content}
                </p>

                </tbody>
        </div>
        <!-- /tab4 end -->
    </fieldset>
</form>

</body>
</html>
