<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/base.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>网站管理</title>
<link rel="stylesheet" type="text/css" href="${ctximg}/kindeditor/themes/default/default.css" />
<script type="text/javascript" src="${ctximg}/kindeditor/kindeditor-all.js"></script>
<script type="text/javascript">
	var type='';
	$(document).ready(function() {
		type='${type}';
		$("#"+type).attr("href","javascript:void(0)");
	});
	function submit(){
		$("#searchForm").attr("action","${ctx}/admin/websiteProfile/find/"+type+"");
		$("#searchForm").submit();
	}
</script>
</head>
<body>

	<div class="rMain rMain-nb">
		<div class="commonWrap">
			<table class="fullwidth" width="100%" cellspacing="0" cellpadding="0" border="0">
				<thead>
					<tr align="center">
						<td width="30%">
							<span>名称</span>
						</td>
						<td width="70%">
							<span>描述</span>
						</td>
					</tr>
				</thead>
				<form action="?" id="searchForm" method="post">
					<input type="hidden" name="flag" id="flag" value="flag" />
					<input type="hidden" name="type" id="type" value="${type}" />
					<c:if test="${type=='web' }">
						<tbody id="tabS_web" align="center">
						<tr class="odd">
								<td>网站title(网站头部)</td>
								<td>${webSiteMap.web.title}</td>
							</tr>
							<tr>
								<td>网校名称(网站头部)</td>
								<td>${webSiteMap.web.company}</td>
							</tr>
							<tr class="odd">
								<td>网站作者</td>
								<td>${webSiteMap.web.author}</td>
							</tr>
							<tr>
								<td>关键词</td>
								<td>${webSiteMap.web.keywords}</td>
							</tr>
							<tr class="odd">
								<td>描述</td>
								<td>${webSiteMap.web.description}</td>
							</tr>
							<tr>
								<td>联系邮箱(网站底部)</td>
								<td>${webSiteMap.web.email}</td>
							</tr>
							<tr class="odd">
								<td>联系电话(网站底部)</td>
								<td>${webSiteMap.web.phone}</td>
							</tr>
							<tr>
								<td>工作时间(网站底部)</td>
								<td>${webSiteMap.web.workTime}</td>
							</tr>
							<tr>
								<td>地址(网站底部)</td>
								<td>${webSiteMap.web.location}</td>
							</tr>
							<tr class="odd">
								<td>版权以及备案号(网站底部)</td>
								<td>${webSiteMap.web.copyright}</td>
							</tr>
						<tr>
							<td>ico文件</td>
							<td>
								<img class="icoimg" alt="" src="${ctx}/favicon.ico?v=<%=Math.random()*100%>">
							</td>
						</tr>
						<tr  class="odd">
							<td>网站logo</td>
							<td>
								<img alt="" src="<%=staticImage%>${webSiteMap.web.logoUrl}" width="144px" height="90px" />
							</td>
						</tr>
						<tr>
							<td>网站底部二维码</td>
							<td>
								<img alt="" src="<%=staticImage%>${webSiteMap.web.imgUrl}" width="144px" height="90px" />
							</td>
						</tr>
						</tbody>
					</c:if>
				</form>

				<c:if test="${type=='censusCode' }">
					<tbody id="tabS_censusCode" align="center">
						<tr>
							<td>统计代码</td>
							<td>
								<textarea rows="6" cols="60" disabled="disabled">${webSiteMap.censusCode.censusCodeString}</textarea>
							</td>
						</tr>
					</tbody>
				</c:if>
				<tr>
					<td colspan="2" align="center">
						<input class="button" type="button" value="修改" onclick="submit(-1)">
						<input class="button" type="button" value="返回" onclick="window.location.href='${ctx}/index.html'">
					</td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>