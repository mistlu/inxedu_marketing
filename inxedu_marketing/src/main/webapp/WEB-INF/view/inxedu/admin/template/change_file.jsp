<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/base.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>修改模板文件</title>
    <link rel="stylesheet" type="text/css" href="${ctx }/static/common/bigcolorpicker/jquery.bigcolorpicker.css"/>
</head>
<body>
    <p>修改模板文件</p>
    <p>文件名：
        <input id="fileName" type="text" value="${name}"/>
        <input type="button" id="changeName" value="修改名字" class="button" onclick="changeName()" />
    </p>
    <textarea id="str" style="width: 100%;height: 500px"><c:out value="${str}"></c:out></textarea>
    <p>
        <input type="button" value="保 存" class="button" onclick="ifExist()" />
        <input type="button" id="deleteFile" value="删 除" class="button" onclick="deleteFile()" />
        <input type="button" value="复 制" class="button" onclick="copyFile()" />
        <input type="button" value="移 动" class="button" onclick="moveFile()" />
        <input type="button" value="还 原" class="button" onclick="recoveryThis()" />
        <input type="button" value="着 色" class="button" onclick="changeColor()" />
        <input type="button" value="返 回" class="button" onclick="javascript:history.go(-1);" />
    </p>
    <script>

        var originalTemplet = '${originalTemplet}';
        if (originalTemplet=='true'){
            $("#changeName").removeAttr("onclick")
            $("#deleteFile").removeAttr("onclick")
            $("#changeName").css({"background":"gray","border":"1px solid gray"})
            $("#deleteFile").css({"background":"gray","border":"1px solid gray"})
            $('#deleteFile').hover(function () {
                $("#changeName").css({"color":"#000"})
            },function () {
                
            })
            $('#changeName').unbind("mouseenter").unbind("mouseleave");

        }
        function moveFile() {
            var path = '${path}';
            var oldName = '${name}';
            $.ajax({
                url:baselocation+'/admin/template/ajax/moveFile',
                type:'post',
                dataType:'json',
                data:{"path":path,"oldName":oldName},
                success:function(result) {
                    if (result.success){
                        window.location.href=baselocation+"/admin/template/file/list";
                    }
                }
            });
        }
        //复制文件
        function copyFile() {
            var str = $("#str").val();
            var name = $("#fileName").val();
            var path = '${path}';
            var oldName = '${name}';

            $.ajax({
                url:baselocation+'/admin/template/ajax/copyFile',
                type:'post',
                dataType:'json',
                data:{"str":str,"path":path,"name":name,"oldName":oldName},
                success:function(result) {
                    if (result.success){
                        window.location.href=baselocation+"/admin/template/file/toChildList?path="+result.message;
                    }
                }
            });
        }
        //修改名字
        function changeName() {
            var str = $("#str").val();
            var name = $("#fileName").val();
            var path = '${path}';
            var oldName = '${name}';

            $.ajax({
                url:baselocation+'/admin/template/ajax/changeName',
                type:'post',
                dataType:'json',
                data:{"str":str,"name":name,"path":path,"oldName":oldName},
                success:function(result) {
                    if (result.success){
                        window.location.href=baselocation+"/admin/template/file/toChildList?path="+result.message;
                    }
                }
            });
        }
        //查看文件是否已存在
        function ifExist() {
            var name = $("#fileName").val();
            var path = "${path}";
            var oldName = '${name}';
            $.ajax({
                url:baselocation+'/admin/template/ajax/ifExist',
                type:'post',
                dataType:'json',
                data:{"path":path,"name":name,"oldName":oldName},
                success:function(result) {
                    if (result.success){
                        changeFile();
                    }else {
                        if (confirm(result.message)){
                            changeFile();
                        }
                    }
                }
            });
        }
        //修改文件
        function changeFile(){
            var str = $("#str").val();
            var name = $("#fileName").val();
            var oldName = '${name}';
            var path = "${path}";
            $.ajax({
                url:baselocation+'/admin/template/ajax/saveFile',
                type:'post',
                dataType:'json',
                data:{"str":str,"path":path,"name":name,"oldName":oldName},
                success:function(result) {
                    if (result.success){
                        window.location.href=baselocation+"/admin/template/file/toChildList?path="+result.message;
                    }
                }
            });
        }
        //删除文件
        function deleteFile() {
            var name =  $("#fileName").val();
            var path = '${path}';
            var oldName = '${name}';
            $.ajax({
                url:baselocation+'/admin/template/ajax/deleteFile',
                type:'post',
                dataType:'json',
                data:{"path":path,"name":name,"oldName":oldName},
                success:function(result) {
                    if (result.success){
                        window.location.href=baselocation+"/admin/template/file/toChildList?path="+result.message;
                    }else {
                        msgshow(result.message,'false');
                    }

                }
            });
        }
        /*文字着色*/
        function changeColor() {
            var path = '${path}';
            window.location.href=baselocation+"/admin/template/toChangeColor?path="+path;
        }
        function recoveryThis() {
            var filePath = '${path}';
            $.ajax({
                url:baselocation+'/admin/template/ajax/recoveryThis',
                type:"post",
                dataType:"json",
                data:{"filePath":filePath},
                success:function (result) {
                    if (result.success){
                        $("#str").val(result.entity.toString())
                    }
                    msgshow(result.message,'false');
                }
            })
        }

    </script>
</body>
</html>
