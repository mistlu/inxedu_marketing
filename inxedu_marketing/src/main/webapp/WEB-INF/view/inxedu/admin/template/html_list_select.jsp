<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/8/30
  Time: 11:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/base.jsp"%>
<html>
<head>
    <title>选择模板</title>
    <style>
        img{
            width: 64px;
            height: 64px;
        }
        .fileLi{
            display: inline;

        }
        ol{
            margin-top: 30px;
        }
        .file{
            width: 80px;
            height: 100px;
            float: left;
            margin: 20px;
        }
        .file-name{
            text-align: center;
        }
    </style>

    <script>
        /**
         * 确认选择
         */
        function confirmSelect(){
            var htmlFile = $("input[name='htmlId']:checked");
            if(htmlFile==null || htmlFile.length==0){
                msgshow("请选择模板");
                return false;
            }
            var htmlFileArr = [];
            var nameArr = [];
            for(var i=0;i<htmlFile.length;i++){
                var htmlFileUrl = $(htmlFile[i]).val();
                var htmlFileName = $(htmlFile[i]).attr('title');
                var file = {'htmlFileUrl':htmlFileUrl,'htmlFileName':htmlFileName};
                htmlFileArr.push(file);
                nameArr.push(htmlFileName);
            }
            window.opener.addListCallback(htmlFileArr);
            closeWin();
        }

        /**
         * 关闭窗口
         */
        function closeWin(){
            window.close();
        }

    </script>
</head>
<body>
<div>
    <ol>
        <c:forEach items="${htmlName}" var="htmlName" varStatus="varStatus">
            <div class="file">
                <li class="fileLi">
                    <a href="javascript: void(0)">
                        <img src="../../../../../images/wenjian.jpg">
                        <input type="checkbox" name="htmlId" title="${htmlName.key}" value="${htmlName.value}" />
                        <p class="file-name" id="file${varStatus.index}"class="fileName">${htmlName.key}</p>
                    </a>
                </li>
            </div>
        </c:forEach>
    </ol>
</div>


    <div style="text-align: center; ">
        <a title="确认" onclick="confirmSelect()" class="button tooltip" href="javascript:void(0)">
            <span></span>
            确认
        </a>
        <a title="关闭" onclick="closeWin()" class="button tooltip" href="javascript:void(0)">
            <span class="ui-icon ui-icon-cancel"></span>
            关闭
        </a>
    </div>
</body>
</html>
