<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/base.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>添加页面</title>
	<%--验证框架--%>
	<link rel="stylesheet" href="${ctx}/static/common/nice-validator/jquery.validator.css"/>
	<script type="text/javascript" src="${ctx}/static/common/nice-validator/jquery.validator.js"></script>
	<script type="text/javascript" src="${ctx}/static/common/nice-validator/local/zh-CN.js"></script>
<script type="text/javascript">
function formSubmit(){
	if(isEmpty($("#fileList").html())){
		msgshow("请选择模板！");
		return;
	}

	var fileType = $("#publishUrl").val();
	if(isEmpty(fileType)){
		msgshow("请填写页面路径!");
		return;
	}
	if (!isEmpty(fileType)&&fileType.substring(fileType.lastIndexOf("."))!=".html"){
		msgshow("文件必须为html格式!");
		return;
	}
	$("#addForm").submit();
}

/**
 * 选择模板
 */
function selectTemplate(){
	window.open(baselocation+'/admin/template/file/selecthtmlList','newwindow', 'toolbar=no,scrollbars=yes,location=no,resizable=no,top=150,left=300,width=923,height=600');
}

/**
 * 选择模板后的回调
 * @param arrList 数组
 */
function addListCallback(arrList){
	var dataHtml='';
	var ascSort=0;
	for(var i=0;i<arrList.length;i++){
		if($("#"+arrList[i].htmlFileName.replace(".","")).length<=0){
			ascSort++;
			//获取最大的排序值
			var maxSort=$('#fileList p:last').find(".hiddenSort").val();
			if(maxSort==undefined){
				maxSort=0;
			}
			dataHtml+='<p style="margin: 0 0 0em;">'+arrList[i].htmlFileName+
			'&nbsp;&nbsp;&nbsp;&nbsp;' +
			'<button type="button" class="ui-state-default ui-corner-all" style="float: none; " onclick="moveUp(this)">上移</button>' +
			'<button type="button" class="ui-state-default ui-corner-all" style="float: none; " onclick="moveDown(this)">下移</button>' +
			'<input value="'+(ascSort+parseInt(maxSort))+'" name="templateSort" class="hiddenSort" type="hidden">' +
			'<input value="'+arrList[i].htmlFileName+'" name="templateName" type="hidden">' +
			'<input value="'+arrList[i].htmlFileUrl+'" id="'+arrList[i].htmlFileName.replace(".","")+'" name="templateUrl" type="hidden">' +
			'<button type="button" class="ui-state-default ui-corner-all" style="float: none; " onclick="$(this).parent().remove()">删除</button></p>';

		}

	}
	$("#fileList").append(dataHtml);
}


//上移操作
function moveUp(obj){
	var current_sort= $(obj).parent().find(".hiddenSort").val();

	var other_sort= $(obj).parent().prev().find(".hiddenSort").val();

	if(other_sort!=undefined){
		$(obj).parent().find(".hiddenSort").val(other_sort);
		$(obj).parent().prev().find(".hiddenSort").val(current_sort);

		$(obj).parent().insertBefore($(obj).parent().prev());    //移动节点
	}
}

//下移操作
function moveDown(obj){
	var current_sort= $(obj).parent().find(".hiddenSort").val();

	var other_sort= $(obj).parent().next().find(".hiddenSort").val();

	if(other_sort!=undefined){
		$(obj).parent().find(".hiddenSort").val(other_sort);
		$(obj).parent().next().find(".hiddenSort").val(current_sort);

		$(obj).parent().next().insertBefore($(obj).parent());    //移动节点
	}
}
</script>
</head>
<body>
<div class="rMain rMain-nb">
	<fieldset>
		<%--<legend>
			<span>页面管理</span>
			&gt;
			<span>添加页面</span>
		</legend>--%>
		<!-- /tab4 begin -->
		<div class="mt20">
			<div class="commonWrap">
				<form action="${ctx}/admin/webpage/add" method="post" id="addForm">
					<input type="hidden" name="isPublish" id="isPublish" value="false" />
					<p class="tit-p">
						<span>
								页面基本属性
								<tt class="c_666 ml20 fsize12">
									（<font color="red">*</font>&nbsp;为必填项）
								</tt>
							</span>
						<span class="field_desc"></span>
					</p>
					<p>
						<label><font color="red">*</font>页面名称</label>
						<input type="text" name="webpage.pageName" class="lf" data-rule="required;" id="pageName" value="" />
						<span class="field_desc"></span>
					</p>
					<p>
						<label >标题</label>
						<input type="text" name="webpage.title" class="lf" id="title" value="" />
						<span class="field_desc">(默认为网站配置)</span>
					</p>
					<p>
						<label >作者</label>
						<input type="text" name="webpage.author" class="lf" id="author" value="" />
						<span class="field_desc">(默认为网站配置)</span>
					</p>
					<p>
						<label >关键词</label>
						<input type="text" name="webpage.keywords" class="lf" id="keywords" value="" />
						<span class="field_desc">(默认为网站配置)</span>
					</p>
					<p>
						<label >描述</label>
						<input type="text" name="webpage.description" class="lf" id="description" value="" />
						<span class="field_desc">(默认为网站配置)</span>
					</p>
					<p>
						<label ><font color="red">*</font>模板</label>
						<input type="button" class="btn btn-success" value="选择模板" onclick="selectTemplate()">
						<span class="field_desc"></span>
					</p>
					<p>
						<label  style="float: left;line-height: 10px;">&nbsp;</label>
						<span id="fileList" class="sf optionClass" style="float:left ;"></span>
						<span class="field_desc"></span>
					</p>
					<p style="clear: both;">
						<label ><font color="red">*</font>发布路径</label>
						<input type="text" name="webpage.publishUrl" class="lf" id="publishUrl" value="" />
						<span class="field_desc">(默认为根目录,多个文件夹用/隔开，路径格式如 <font color="red">/hello.html、/pages/hello.html</font>)</span>
					</p>
					<p>
						<input type="button" value="保 存" class="button" onclick="formSubmit()" />
						<input type="button" value="保存并发布" class="button" onclick="$('#isPublish').val('true');formSubmit()" />
					</p>
				</form>
			</div>
		</div>
	</fieldset>
</div>
</body>
</html>
