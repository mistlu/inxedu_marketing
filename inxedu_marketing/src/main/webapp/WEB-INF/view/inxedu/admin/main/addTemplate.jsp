<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta -->
    <meta charset="utf-8" http-equiv="Content-Type" />
    <!-- End of Meta -->
    <!-- Page title -->
    <title>${websitemap.web.company}-${websitemap.web.title}</title>
    <!-- End of Page title -->
    <meta name="author" content="${websitemap.web.author}" />
    <meta name="keywords" content="${websitemap.web.keywords}" />
    <meta name="description" content="${websitemap.web.description}" />
    <link rel="stylesheet" type="text/css" href="${ctx}/static/admin/css/jquery.lightbox.css">
    <script type="text/javascript" src="${ctximg}/static/common/jquery.lightbox.min.js"></script>
    <script>
        function confirmSelect(templateName,templateUrl,webpageId){
            if (confirm('是否添加模板')){
                window.parent.appendTemplate(templateName,templateUrl,webpageId);
            }
        }
        function tow(templateName1,templateUrl1,templateName2,templateUrl2,webpageId) {
            if (confirm('是否添加模板')){
                window.parent.appendTemplate(templateName1,templateUrl1,webpageId);
                window.parent.appendTemplate(templateName2,templateUrl2,webpageId);
            }
        }
        $(function () {
            var templateUrlArr = '${templateUrlArr}';
            templateUrlArr = templateUrlArr.substring(1,templateUrlArr.length-1);
            templateUrlArr = templateUrlArr.replace(/\s/g, "");
            var urlArr = templateUrlArr.split(",");
            for (i=0;i<urlArr.length;i++){
                if (urlArr[i]=='/template/templet1/首页_课程介绍模板3.html'){
                    urlArr[i]='shouyekechengjieshao3';
                }
                if (urlArr[i]=='/template/templet1/首页_课程介绍模板4.html'){
                    urlArr[i]='shouyekechengjieshao4';
                }
                if (urlArr[i]=='/template/templet1/课程_精品课程.html'){
                    urlArr[i]='kechengjingpinkecheng';
                }
                if (urlArr[i]=='/template/templet1/首页_新闻资讯.html'){
                    urlArr[i]='shouyexinwenzixun';
                }
                if (urlArr[i]=='/template/templet1/首页_banner模板2.html'){
                    urlArr[i]='shouyebanner2';
                }
                if (urlArr[i]=='/template/templet1/首页_banner模板3.html'){
                    urlArr[i]='shouyebanner3';
                }
                if (urlArr[i]=='/template/templet1/首页_新闻资讯模板4.html'){
                    urlArr[i]='shouyexinwenzixun4';
                }
                if (urlArr[i]=='/template/templet1/资讯_资讯列表.html'){
                    urlArr[i]='zixunzixunliebiao';
                }
                if (urlArr[i]=='/template/templet1/首页_师资力量.html.html'){
                    urlArr[i]='shizijiangshituandui';
                }
                if (urlArr[i]=='/template/templet1/首页_师资力量模板2.html'){
                    urlArr[i]='shouyeshizililiang2';
                }
                if (urlArr[i]=='/template/templet1/首页_师资团队模板3.html'){
                    urlArr[i]='shouyeshizituandui3';
                }
                if (urlArr[i]=='/template/templet1/首页_师资团队模板4.html'){
                    urlArr[i]='shouyeshizituandui4';
                }
                if (urlArr[i]=='/template/templet1/首页_关于我们.html'){
                    urlArr[i]='shouyeguanyuwomen';
                }
                if (urlArr[i]=='/template/templet1/关于我们_我们是谁.html'){
                    urlArr[i]='guanyuwomenwomenshishei';
                }
                if (urlArr[i]=='/template/templet1/关于我们_我们的优势.html'){
                    urlArr[i]='guanyuwomenwomendeyoushi';
                }
                if (urlArr[i]=='/template/templet1/关于我们_我们的服务.html'){
                    urlArr[i]='guanyuwomenwomendefuwu';
                }
                urlArr[i] = urlArr[i].replace("/template/templet1/关于我们_找到我们.html","guanyuwomenzhaodaowomen");
                urlArr[i] = urlArr[i].replace("/template/templet1/首页_考试系统模板2.html","shouyekaoshixitong2");
                urlArr[i] = urlArr[i].replace("/template/templet1/首页_在线学习模板2.html","shouyezaixianxuexi2");
                urlArr[i] = urlArr[i].replace("/template/templet1/课程_优势介绍.html","kechengyoushijieshao");
                urlArr[i] = urlArr[i].replace("/template/templet1/师资_学员心声.html","shizixueyuanxinsheng");
                urlArr[i] = urlArr[i].replace("/template/templet1/师资_优势介绍.html","shiziyoushijieshao");
                urlArr[i] = urlArr[i].replace("/template/templet1/首页_留学资讯模板5.html","shouyexinwenzixun5");
                urlArr[i] = urlArr[i].replace("/template/templet1/首页_职业资讯模板6.html","shouyexinwenzixun6");
                urlArr[i] = urlArr[i].replace("/template/templet1/首页_关于我们模板5.html","shouyeguanyuwomen5");
                urlArr[i] = urlArr[i].replace("/template/templet1/首页_迎战未来的4大因素6.html","shouyeyingzhanweilai");
                urlArr[i] = urlArr[i].replace("/template/templet1/首页_生活环境模板6.html","shouyeguanyuwomen6");
                urlArr[i] = urlArr[i].replace("/template/templet1/首页_优秀学员模板5.html","shouyeyouxiuxueyuan6");
                urlArr[i] = urlArr[i].replace("/template/templet1/首页_课程分类模板5.html","shouyekechengfenlei5");
                var id = "#"+urlArr[i];
                $("#"+urlArr[i]).addClass("current");
            }
        })
    </script>
</head>
<body>
    <div class="rMain">
        <div>
            <p class="hLh30 fsize20 c-333 f-fH ">课程模板</p>
            <div class="mb-wrap mt30">
                <ul class="clearfix mb-list mb-fl-list">
                    <li id="shouyekechengjieshao3">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-kc-1.jpg" rel="lightbox[plants]" title="课程模块一" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-kc-1.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">课程模块一</h5>
                                <p class="txt-nr">
                                    图片规格：640px*357px，（建议图片大小100kb以内！）
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('课程模块一','/template/templet1/首页_课程介绍模板3.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="shouyekechengjieshao4">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-kc-2.jpg" rel="lightbox[plants]" title="课程模块二" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-kc-2.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">课程模块二</h5>
                                <p class="txt-nr">
                                    图片规格：640px*357px，（建议图片大小100kb以内！）
                                </p>
                            </div>
                        </div>

                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('课程模块二','/template/templet1/首页_课程介绍模板4.html',${webpageId})"class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="kechengjingpinkecheng">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-kc-3.jpg" rel="lightbox[plants]" title="课程模块三" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-kc-3.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">课程模块三</h5>
                                <p class="txt-nr">
                                    图片规格：640px*357px，（建议图片大小100kb以内！）
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('课程模块三','/template/templet1/课程_精品课程.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="shouyekechengfenlei5">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-kc-4.jpg" rel="lightbox[plants]" title="课程模块四" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-kc-4.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">课程模块四</h5>
                                <p class="txt-nr">
                                    图片规格：185px*205px，（建议图片大小100kb以内！）
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('课程模块三','/template/templet1/首页_课程分类模板5.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div>
            <p class="hLh30 fsize20 c-333 f-fH  mt30">资讯模板</p>
            <div class="mb-wrap mt30">
                <ul class="clearfix mb-list mb-fl-list">
                    <li id="shouyexinwenzixun">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-zx-1.jpg" rel="lightbox[plants]" title="资讯模块一" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-zx-1.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">资讯模块一</h5>
                                <p class="txt-nr">
                                    图片规格：500px*332px，（建议图片大小100kb以内！）
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('资讯模块一','/template/templet1/首页_新闻资讯.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="shouyebanner2">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-zx-2.jpg" rel="lightbox[plants]" title="资讯模块二" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-zx-2.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">资讯模块二</h5>
                                <p class="txt-nr">
                                    图片规格：500px*332px，（建议图片大小100kb以内！）
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('资讯模块二','/template/templet1/首页_banner模板2.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="shouyebanner3">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-zx-3.jpg" rel="lightbox[plants]" title="资讯模块三" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-zx-3.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">资讯模块三</h5>
                                <p class="txt-nr">
                                    图片规格：500px*332px，（建议图片大小100kb以内！）
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('资讯模块三','/template/templet1/首页_banner模板3.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                   <%-- <li id="shouyexinwenzixun4">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-zx-4.jpg" rel="lightbox[plants]" title="资讯模块四" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-zx-4.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">资讯模块四</h5>
                                <p class="txt-nr">
                                    图片规格：500px*332px，（建议图片大小100kb以内！）
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            &lt;%&ndash;<a href="javascript:void(0)"onclick="tow('资讯模块四资讯','/template/templet1/首页_新闻资讯模板4.html','资讯模块四话题','/template/templet1/首页_热门话题模板4.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>&ndash;%&gt;
                            <a href="javascript:void(0)"onclick="confirmSelect('资讯模块四资讯','/template/templet1/首页_新闻资讯模板4.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>--%>
                    <li id="zixunzixunliebiao">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-zx-5.jpg" rel="lightbox[plants]" title="资讯模块五" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-zx-5.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">资讯模块四</h5>
                                <p class="txt-nr">
                                    图片规格：500px*332px，（建议图片大小100kb以内！）
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('资讯模块五','/template/templet1/资讯_资讯列表.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="shouyexinwenzixun5">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-zx-9.jpg" rel="lightbox[plants]" title="资讯模块六" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-zx-9.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">资讯模块五</h5>
                                <p class="txt-nr">
                                    图片规格：500px*332px，（建议图片大小100kb以内！）
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('资讯模块六','/template/templet1/首页_留学资讯模板5.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="shouyexinwenzixun6">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-zx-7.jpg" rel="lightbox[plants]" title="资讯模块七" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-zx-7.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">资讯模块六</h5>
                                <p class="txt-nr">
                                    图片规格：500px*332px，（建议图片大小100kb以内！）
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('资讯模块七','/template/templet1/首页_职业资讯模板6.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <%--<li id="">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-zx-8.jpg" rel="lightbox[plants]" title="资讯模块八" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-zx-8.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">资讯模块八</h5>
                                <p class="txt-nr">
                                    图片规格：500px*332px，（建议图片大小100kb以内！）
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('资讯模块五','/template/templet1/资讯_资讯列表.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>--%>
                </ul>
            </div>
        </div>
        <div>
            <p class="hLh30 fsize20 c-333 f-fH  mt30">师资模板</p>
            <div class="mb-wrap mt30">
                <ul class="clearfix mb-list mb-fl-list">
                    <li id="shizijiangshituandui">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-sz-1.jpg" rel="lightbox[plants]" title="师资模块一" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-sz-1.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">师资模块一</h5>
                                <p class="txt-nr">
                                    图片规格：500px*500px，（建议图片大小100kb以内！）
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('师资模块一','/template/templet1/首页_师资力量.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="shouyeshizililiang2">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-sz-2.jpg" rel="lightbox[plants]" title="师资模块二" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-sz-2.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">师资模块二</h5>
                                <p class="txt-nr">
                                    图片规格：500px*500px，（建议图片大小100kb以内！）
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('师资模块二','/template/templet1/首页_师资力量模板2.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="shouyeshizituandui3">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-sz-3.jpg" rel="lightbox[plants]" title="师资模块三" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-sz-3.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">师资模块三</h5>
                                <p class="txt-nr">
                                    图片规格：500px*500px，（建议图片大小100kb以内！）
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('师资模块三','/template/templet1/首页_师资团队模板3.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="shouyeshizituandui4">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-sz-4.jpg" rel="lightbox[plants]" title="师资模块四" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-sz-4.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">师资模块四</h5>
                                <p class="txt-nr">
                                    图片规格：500px*500px，（建议图片大小100kb以内！）
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('师资模块四','/template/templet1/首页_师资团队模板4.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <%--<li id="shouyeshizituandui5">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-sz-5.jpg" rel="lightbox[plants]" title="师资模块五" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-sz-5.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">师资模块五</h5>
                                <p class="txt-nr">
                                    图片规格：500px*500px，（建议图片大小100kb以内！）
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('师资模块四','/template/templet1/首页_师资团队模板4.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>--%>
                </ul>
            </div>
        </div>
        <div>
            <p class="hLh30 fsize20 c-333 f-fH  mt30">关于我们模板</p>
            <div class="mb-wrap mt30">
                <ul class="clearfix mb-list mb-fl-list">
                    <li id="shouyeguanyuwomen">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-us-1.jpg" rel="lightbox[plants]" title="关于我们模块一" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-us-1.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">关于我们模块一</h5>
                                <p class="txt-nr">
                                    图片规格：538px*363px，（建议图片大小100kb以内！）
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('关于我们模块一','/template/templet1/首页_关于我们.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="guanyuwomenwomenshishei">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-us-2.jpg" rel="lightbox[plants]" title="关于我们模块二" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-us-2.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">关于我们模块二</h5>
                                <p class="txt-nr">

                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('关于我们模块二','/template/templet1/关于我们_我们是谁.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="guanyuwomenwomendeyoushi">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-us-3.jpg" rel="lightbox[plants]" title="关于我们模块三" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-us-3.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">关于我们模块三</h5>
                                <p class="txt-nr">
                                    图片规格：52px*52px，png格式图片
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('关于我们模块三','/template/templet1/关于我们_我们的优势.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="guanyuwomenwomendefuwu">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-us-4.jpg" rel="lightbox[plants]" title="关于我们模块四" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-us-4.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">关于我们模块四</h5>
                                <p class="txt-nr">
                                    图片规格：64px*64px，png格式图片
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('关于我们模块四','/template/templet1/关于我们_我们的服务.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="guanyuwomenzhaodaowomen">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-us-5.jpg" rel="lightbox[plants]" title="关于我们模块五" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-us-5.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">关于我们模块五</h5>
                                <p class="txt-nr">

                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('关于我们模块五','/template/templet1/关于我们_找到我们.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="shouyeguanyuwomen5">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-us-6.jpg" rel="lightbox[plants]" title="关于我们模块六" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-us-7.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">关于我们模块六</h5>
                                <p class="txt-nr">

                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('关于我们模块五','/template/templet1/首页_关于我们模板5.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="shouyeguanyuwomen6">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-us-7.jpg" rel="lightbox[plants]" title="关于我们模块七" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-us-6.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">关于我们模块七</h5>
                                <p class="txt-nr">

                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('关于我们模块七','/template/templet1/首页_生活环境模板6.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div>
            <p class="hLh30 fsize20 c-333 f-fH  mt30">其他模板</p>
            <div class="mb-wrap mt30">
                <ul class="clearfix mb-list mb-fl-list">
                    <li id="shouyekaoshixitong2">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-qt-1.jpg" rel="lightbox[plants]" title="其他模块一" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-qt-1.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">其他模块一</h5>
                                <p class="txt-nr">
                                    图片规格：88px*88px，png格式图片
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('其他模块一','/template/templet1/首页_考试系统模板2.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="shouyezaixianxuexi2">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-qt-2.jpg" rel="lightbox[plants]" title="其他模块二" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-qt-2.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">其他模块二</h5>
                                <p class="txt-nr">
                                    图片规格：88px*88px，png格式图片
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('其他模块二','/template/templet1/首页_在线学习模板2.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="kechengyoushijieshao">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-qt-3.jpg" rel="lightbox[plants]" title="其他模块三" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-qt-3.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">其他模块三</h5>
                                <p class="txt-nr">
                                    图片规格：200px*200px，png格式图片
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('其他模块三','/template/templet1/课程_优势介绍.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="shizixueyuanxinsheng">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-qt-4.jpg" rel="lightbox[plants]" title="其他模块四" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-qt-4.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">其他模块四</h5>
                                <p class="txt-nr">
                                    图片规格：500px*500px，jpg格式图片
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('其他模块四','/template/templet1/师资_学员心声.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="shiziyoushijieshao">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-qt-5.jpg" rel="lightbox[plants]" title="其他模块五" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-qt-5.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">其他模块五</h5>
                                <p class="txt-nr">
                                    图片规格：200px*200px，png格式图片
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('其他模块五','/template/templet1/师资_优势介绍.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="shouyeyingzhanweilai">
                        <div class="nr-box">
                            <div class="pic">
                                    <a href="/static/admin/mb-pic/mk-qt-7.jpg" rel="lightbox[plants]" title="其他模块六" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-qt-7.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">其他模块六</h5>
                                <p class="txt-nr">
                                    图片规格：640px*357px，png格式图片
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('其他模块六','/template/templet1/首页_迎战未来的4大因素6.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <%--<li id="shiziyoushijieshao">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-qt-6.jpg" rel="lightbox[plants]" title="其他模块七" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-qt-6.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">其他模块七</h5>
                                <p class="txt-nr">
                                    图片规格：95px*95px，png格式图片
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('其他模块七','/template/templet1/师资_优势介绍.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>--%>
                    <%--<li id="shiziyoushijieshao">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-qt-8.jpg" rel="lightbox[plants]" title="其他模块八" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-qt-8.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">其他模块八</h5>
                                <p class="txt-nr">
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('其他模块五','/template/templet1/师资_优势介绍.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                    <li id="shiziyoushijieshao">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-zx-6.jpg" rel="lightbox[plants]" title="其他模块九" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-zx-6.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">其他模块九</h5>
                                <p class="txt-nr">
                                    图片规格：500px*332px，png格式图片
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('其他模块五','/template/templet1/师资_优势介绍.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>--%>
                    <li id="shouyeyouxiuxueyuan5">
                        <div class="nr-box">
                            <div class="pic">
                                <a href="/static/admin/mb-pic/mk-qt-9.jpg" rel="lightbox[plants]" title="其他模块十" class="lightbox-enabled">
                                    <img src="/static/admin/mb-pic/mk-qt-9.jpg">
                                </a>
                            </div>
                            <div class="mt10 ml10 mr10">
                                <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">其他模块十</h5>
                                <p class="txt-nr">
                                    图片规格：根据实际尺寸定义
                                </p>
                            </div>
                        </div>
                        <div class="tac mt30">
                            <a href="javascript:void(0)"onclick="confirmSelect('其他模块五','/template/templet1/首页_优秀学员模板5.html',${webpageId})" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模块</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</body>
</html>


